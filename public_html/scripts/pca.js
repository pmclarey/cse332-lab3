// dimensional constants
var MARGIN = {TOP: 20, RIGHT: 30, BOTTOM: 30, LEFT: 80};
var WIDTH = 960 - MARGIN.LEFT - MARGIN.RIGHT;
var HEIGHT = 640 - MARGIN.TOP - MARGIN.BOTTOM;

var data = [{name:"1", value:69.8530373},
    {name:"2", value:26.0983763},
    {name:"3", value:3.60675969},
    {name:"4", value:0.327128917},
    {name:"5", value:0.0937447348},
    {name:"6", value:0.0133084496},
    {name:"7", value:0.00268817393},
    {name:"8", value:0.00247300111},
    {name:"9", value:0.00201355016},
    {name:"10", value:0.000469940701}];

function drawChart() {
    var domain = [];
    data.forEach(function(d) {
        domain.push(d.name); 
    });
    
    var yScale = d3.scale.linear()
            .domain([0, 100])
            .range([HEIGHT, 0]);

    var xScale = d3.scale.ordinal()
            .domain(domain)
            .rangeBands([0, WIDTH], 0.5);
    
    var yAxis = d3.svg.axis()
            .scale(yScale)
            .orient('left');
    
    var xAxis = d3.svg.axis()
            .scale(xScale);

    chart = d3.select('.chart')
            .attr('width', WIDTH + MARGIN.LEFT + MARGIN.RIGHT)
            .attr('height', HEIGHT + MARGIN.TOP + MARGIN.BOTTOM)
            .append("g")
            .attr("transform", "translate(" + MARGIN.LEFT + "," + MARGIN.TOP + ")");
    
    chart.append('g')
            .attr("class", "x axis")
            .attr("transform", "translate(0," + HEIGHT + ")")
            .call(xAxis)
        .selectAll("text")
            .attr("transform", "rotate(-45) translate(-5, -5)")
            .style("text-anchor", "end");
    
    chart.append('g')
            .attr('class', 'y axis')
            .call(yAxis);
    
    bars = chart.selectAll('rect')
            .data(data)
            .enter().append('rect')
            .attr('width', xScale.rangeBand())
            .attr('x', function(d) {
                return xScale(d.name);
            })
            .attr('height', function(d) {
                return yScale(0) - yScale(d.value);
            })
            .attr('y', function(d) {
                return yScale(d.value);
            });
    
    textValues = chart.selectAll('text.value')
            .data(data)
            .enter().append('text')
            .style('visibility', 'hidden')
            .style('text-anchor', 'middle')
            .style('fill', 'white')
            .style('font', '18pt "Comic-Sans"')
            .attr('x', function(d) {
                return xScale(d) + xScale.rangeBand() / 2;
            })
            .attr('y', function(d) {
                return yScale(d.value + 100 * 0.1) + 40;
            })
            .text(function(d) {
                return d.value;
            });
    /*
    bars.transition()
            .attr('height', function(d) {
                return yScale(0) - yScale(d.value);
            })
            .attr('y', function(d) {
                return yScale(d.value);
            })
            .duration(2000)
            .ease('bounce')
            .each('end', function() {
                textValues.style('visibility', 'visible');
            });
            
    bars.on('mouseover', function() {
        d3.select(this)
                .transition()
                .attr('width', xScale.rangeBand() + 20 )
                .attr('x', function(d) {
                    return xScale(d) - 10;
                })
                .attr('height', function(d) {
                    return yScale(0) - yScale(d + maxVal * 0.1);
                })
                .attr('y', function(d) {
                    return yScale(d + maxVal * 0.1);
                });
    });
    
    bars.on('mouseout', function() {
        d3.select(this)
                .transition()
                .attr('width', xScale.rangeBand())
                .attr('x', function(d) {
                    return xScale(d.name);
                })
                .attr('height', function(d) {
                    return yScale(0) - yScale(d.value);
                })
                .attr('y', function(d) {
                    return yScale(d.value);
                });
    }); */
}

window.onload = drawChart;
