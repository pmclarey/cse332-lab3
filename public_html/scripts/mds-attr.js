var svg;
var width;
var height;

function draw(data, xAttr, yAttr) {
    var xValue = function(d) { return d[xAttr]; },
            xScale = d3.scale.linear().range([0, width]),
            xMap = function(d) { return xScale(xValue(d));},
            xAxis = d3.svg.axis().scale(xScale).orient("bottom");
    
    var yValue = function(d) { return d[yAttr]; },
            yScale = d3.scale.linear().range([height, 0]),
            yMap = function(d) { return yScale(yValue(d));},
            yAxis = d3.svg.axis().scale(yScale).orient("left");
    
    var xMin = d3.min(data, xValue);
    var xMax = d3.max(data, xValue);
    var xRange = xMax - xMin;
    var yMin = d3.min(data, yValue);
    var yMax = d3.max(data, yValue);
    var yRange = yMax - yMin;
    xMin = xMin - (xRange * 0.1);
    xMax = xMax + (xRange * 0.1);
    yMin = yMin - (yRange * 0.1);
    yMax = yMax + (yRange * 0.1);
    xScale.domain([xMin, xMax]);
    yScale.domain([yMin, yMax]);
    
    // x-axis
    svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)
        .append("text")
            .attr("class", "label")
            .attr("x", width)
            .attr("y", -6)
            .style("text-anchor", "end")
            .text(xAttr);

    // y-axis
    svg.append("g")
            .attr("class", "y axis")
            .call(yAxis)
        .append("text")
            .attr("class", "label")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text(yAttr);

    // draw dots
    svg.selectAll(".dot")
            .data(data)
            .enter().append("circle")
            .attr("class", "dot")
            .attr("r", 3.5)
            .attr("cx", xMap)
            .attr("cy", yMap)
            .style("fill", "steelblue");
    
    svg.selectAll(".name")
            .data(data)
            .enter().append("text")
            .attr("x", xMap)
            .attr("y", yMap)
            .attr("transform", "translate(0,-10)")
            .text(function(d) {
                alert(d["name"]);
                return d["name"];
            });
}

function init() {
    var margin = {top: 20, right: 20, bottom: 30, left: 80};
    width = 1800 - margin.left - margin.right;
    height = 1000 - margin.top - margin.bottom;
            
    svg = d3.select("#chart").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + ","
                    + margin.top + ")");
    
    d3.csv("./data/mds_attr.csv", function(data) {
        data.forEach(function(d) {
            d["name"] = d[""];
            d["x"] = +d["x"];
            d["y"] = +d["y"];
        });
        
        draw(data, "x", "y");
    });  
}

window.onload = init;
