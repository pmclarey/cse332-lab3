function draw(data) {
    
    // standard d3 plot setup
    var margin = {
        top: 160,
        right: 160,
        bottom: 160,
        left: 160
    };
    var width = 1000 - margin.left - margin.right;
    var height = 1000 - margin.top - margin.bottom;
    var domain = d3.set(data.map(function(d) { // our domain is just the column names
        return d.x;
    })).values();
    var num = Math.sqrt(data.length);   // how many rows and columns
    var color = d3.scale.linear()       // our color scale from blue to white to red
        .domain([-1, 0, 1])
        .range(["#000080", "#fff", "#B22222"]);
    
     // set-up x and y scale
    var x = d3.scale
            .ordinal()
            .rangePoints([0, width])
            .domain(domain);
    var y = d3.scale
            .ordinal()
            .rangePoints([0, height])
            .domain(domain);
    var xSpace = x.range()[1] - x.range()[0]; // this is the space of each grid space
    var ySpace = y.range()[1] - y.range()[0];

    var svg = d3.select("body")
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    
    // bind our data for each grid space
    var cor = svg.selectAll(".cor")
            .data(data)
            .enter()
            .append("g")
            .attr("class", "cor")
            .attr("transform", function(d) {
                return "translate(" + x(d.x) + "," + y(d.y) + ")";
            });

    // outer rectangle on each grid space
    cor.append("rect")
            .attr("width", xSpace)
            .attr("height", ySpace)
            .attr("x", -xSpace / 2)
            .attr("y", -ySpace / 2)
            .style("fill", function(d){
                return color(d.value);
            });

    // build the "yAxis" color scale
    // its a series of rects colored correctly
    // to produce a smooth gradient
    var aS = d3.scale
            .linear()
            .range([-margin.top + 5, height + margin.bottom - 5])
            .domain([1, -1]);

    var yA = d3.svg.axis()
            .orient("right")
            .scale(aS)
            .tickPadding(7);

    var aG = svg.append("g")
            .attr("class", "y axis")
            .call(yA)
            .attr("transform", "translate(" + (width + margin.right / 2) + " ,0)");

    var iR = d3.range(-1, 1.01, 0.01);
    var h = height / iR.length + 3;
    iR.forEach(function(d){
        aG.append('rect')
                .style('fill',color(d))
                .style('stroke-width', 0)
                .style('stoke', 'none')
                .attr('height', h)
                .attr('width', 10)
                .attr('x', 0)
                .attr('y', aS(d));
   });
   
    var yAxis = d3.svg.axis()
            .scale(y)
            .orient('left');
    
    var xAxis = d3.svg.axis()
            .scale(x);
    
    svg.append('g')
            .attr('class', 'y axis')
            .attr("transform", "translate(0," + (height + 35) + ")")
            .call(xAxis)
        .selectAll("text")
            .attr("transform", "rotate(-45) translate(-25, -5)")
            .style("text-anchor", "end");
    
    svg.append('g')
            .attr('class', 'y axis')
            .attr("transform", "translate(-35,0)")
            .call(yAxis);
}

function init() {
    d3.csv("./data/corrcoef.csv", function(rows) {
        var data = [];
        
        rows.forEach(function(d) {
            var x = d[""];
            delete d[""];
            for (attr in d) {
                var y = attr;
                value = d[attr];
                data.push({
                   x: x,
                   y: y,
                   value: +value
                });
            }
        });
        
        draw(data);
    });
}

window.onload = init;
